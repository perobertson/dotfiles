~/.local/bin/user-updates:
	# if this fails too install, make sure `openssl-devel` is installed
	# https://github.com/sfackler/rust-openssl/blob/master/openssl/src/lib.rs#L31
	cargo install --git "https://gitlab.com/perobertson-tools/user-updates.git"

.PHONY: install
install: ~/.local/bin/user-updates
	./install.py -v
	~/.local/bin/user-updates config add-repo "https://gitlab.com/perobertson/user-updates-contents.git"
	~/.local/bin/user-updates config add-repo "https://gitlab.com/perobertson/dotfiles.git"
	~/.local/bin/user-updates apply
