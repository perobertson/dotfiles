#!/usr/bin/env bash
# This file is generated by perobertson/dotfiles
# Manual edits will be lost in future updates
#
# dkms is used to allow automatic kernel updates. It needs to be installed prior
# to installing the nvidia drivers. The system also needs to be rebooted once
# dkms is installed so that it is loaded and listening for the proper hooks.
#
# SecureBoot is a requirement for Windows11. To support dual booting the nvidia
# driver needs to be signed so that it is loaded during system boot. Driver
# signing is supported by dkms and the driver signing keys are configured in
# /etc/dkms/framework.conf
#
# For the system to load the self signed driver the key needs to be registered
# on the system. This is done by using mokutil.
#
# To clean up old self signing keys you need to use
# mokutil --delete /path/to/public.key
# If you no longer have the public keys, use "mokutil --export" to get them.
#
# Troubleshooting
#
# ```bash
# dkms status # list kernel modules; check for build errors
# lsmod | grep nvidia # see which modules are loaded; compare to last output
# mokutil --list-enrolled # see which keys are registered on the machine
# ```
#
# References:
#
# https://docs.fedoraproject.org/en-US/fedora/latest/system-administrators-guide/kernel-module-driver-configuration/Working_with_Kernel_Modules/
# https://www.if-not-true-then-false.com/2015/fedora-nvidia-guide/
# https://blog.monosoul.dev/2021/12/29/automatically-sign-nvidia-kernel-module-in-fedora/
# https://blog.monosoul.dev/2022/05/17/automatically-sign-nvidia-kernel-module-in-fedora-36/
# https://www.linuxcapable.com/how-to-install-nvidia-drivers-on-fedora-linux/
# - this did not seem to work

set -euo pipefail

generate_signing_key(){
    if sudo bash -c '[[ -f /var/lib/dkms/mok-perobertson.key ]]'; then
        echo "Driver signing key already exists"
        return 1
    elif sudo bash -c '[[ -f /var/lib/dkms/mok-perobertson.pub ]]'; then
        echo "Driver signing key already exists"
        return 2
    fi
    os_info=$(. /etc/os-release; echo $ID-$VERSION_ID)
    sudo openssl req \
        -new \
        -x509 \
        -newkey rsa:4096 \
        -keyout /var/lib/dkms/mok-perobertson.key \
        -outform DER \
        -out /var/lib/dkms/mok-perobertson.pub \
        -nodes \
        -days 36500 \
        -subj "/CN=Private Driver Signing - $os_info"
    sudo tee <<EOF /etc/dkms/framework.conf.d/99-perobertson.conf
mok_signing_key=/var/lib/dkms/mok-perobertson.key
mok_certificate=/var/lib/dkms/mok-perobertson.pub
EOF
}

enroll_signing_key(){
    if sudo bash -c '[[ ! -f /var/lib/dkms/mok-perobertson.pub ]]'; then
        generate_signing_key
    fi
    sudo mokutil --import /var/lib/dkms/mok-perobertson.pub
    echo "Key is enrolled, be sure to reboot before proceeding."
}

install_dependencies(){
    sudo dnf install \
        acpid \
        curl \
        dkms \
        gcc \
        kernel-devel \
        kernel-headers \
        libglvnd-devel \
        libglvnd-glx \
        libglvnd-opengl \
        make \
        mokutil \
        openssl \
        pkgconfig \
        xz \
        zstd
    sudo dnf update
    download_driver
    echo "dependencies installed be sure to reboot"
}

download_driver(){
    # Go here to find the latest versions:
    # https://www.nvidia.com/en-us/drivers/unix/
    # Use: Latest Production Branch Version
    # release date Tue Dec 17, 2024
    local version=550.142
    local filename=NVIDIA-Linux-x86_64-${version}.run
    local driver=/var/local/nvidia-installer/${filename}
    if [[ -x "${driver}" ]]; then
        return
    fi
    sudo mkdir -p /var/local/nvidia-installer
    sudo curl -o "${driver}" \
        "https://us.download.nvidia.com/XFree86/Linux-x86_64/${version}/${filename}"
    sudo chmod +x "${driver}"
}

disable_nouveau(){
    # Check for an available installer before removing the current driver
    installer=$(find /var/local/nvidia-installer -name 'NVIDIA-*.run' | sort -V | tail -n 1)
    if [[ ! -x "${installer}" ]]; then
        echo "No installer found in /var/local/nvidia-installer"
        echo "See: https://www.nvidia.com/en-us/drivers/unix/"
        exit 3
    fi
    if ! grep "blacklist nouveau" /etc/modprobe.d/*; then
        echo "blacklist nouveau" | sudo tee /etc/modprobe.d/disable_nouveau.conf
    fi

    # prevent the nouveau driver from loading
    if ! grep "^GRUB_CMDLINE_LINUX=.*rd.driver.blacklist=nouveau" /etc/default/grub >/dev/null; then
        sudo sed --in-place=.bak 's/^\(GRUB_CMDLINE_LINUX=".*\)"$/\1 rd.driver.blacklist=nouveau"/' /etc/default/grub

        # for both BIOS and UEFI after fedora 34
        sudo grub2-mkconfig -o /boot/grub2/grub.cfg
    fi

    # Uninstall nouveau driver
    sudo dnf remove xorg-x11-drv-nouveau

    # generate_initramfs
    ## Backup old initramfs nouveau image ##
    sudo mv "/boot/initramfs-$(uname -r).img" "/boot/initramfs-$(uname -r)-nouveau.img"

    ## Create new initramfs image ##
    sudo dracut "/boot/initramfs-$(uname -r).img" "$(uname -r)"

    echo "Nouveau has been disabled."
    echo "Changing runlevel to multi-user"
    sudo systemctl set-default multi-user.target
    echo "Runlevel changed. Time to reboot and install the NVIDIA driver."
}

enable_nouveau(){
    if [[ "N 3" != "$(runlevel)" ]]; then
        echo "Changing runlevel to multi-user"
        sudo systemctl set-default multi-user.target
        echo "Please reboot and run this command again"
        return
    fi
    sudo dnf install xorg-x11-drv-nouveau
    sudo rm -f /etc/modprobe.d/disable_nouveau.conf
    if grep "^GRUB_CMDLINE_LINUX=.*rd.driver.blacklist=nouveau" /etc/default/grub >/dev/null; then
        sudo sed --in-place=.bak 's/rd.driver.blacklist=nouveau//' /etc/default/grub
        sudo grub2-mkconfig -o /boot/grub2/grub.cfg
    fi
    sudo mv "/boot/initramfs-$(uname -r).img" "/boot/initramfs-$(uname -r)-nvidia.img"
    sudo dracut "/boot/initramfs-$(uname -r).img" "$(uname -r)"
    echo "Changing runlevel back to graphical"
    sudo systemctl set-default graphical.target
    echo "Runlevel changed. Time to reboot."
}

install_nvidia(){
    if [[ "N 3" != "$(runlevel)" ]]; then
        echo "Changing runlevel to multi-user"
        sudo systemctl set-default multi-user.target
        echo "Please reboot and run this command again"
        return
    fi
    installer=$(find /var/local/nvidia-installer -name 'NVIDIA-*.run' | sort -V | tail -n 1)
    if [[ ! -x "${installer}" ]]; then
        echo "No installer found in /var/local/nvidia-installer"
        echo "See: https://www.nvidia.com/en-us/drivers/unix/"
        exit 3
    fi
    sudo "${installer}" \
        --module-signing-secret-key=/var/lib/dkms/mok-perobertson.key \
        --module-signing-public-key=/var/lib/dkms/mok-perobertson.pub

    echo "NVIDIA has been installed"
    echo "Changing runlevel back to graphical"
    sudo systemctl set-default graphical.target
    echo "Runlevel changed. Time to reboot and install the video acceleration."
}

install_vaapi_video_acceleration(){
    sudo dnf install \
        libva-nvidia-driver \
        libva-utils
    # https://wiki.archlinux.org/title/Hardware_video_acceleration#Configuring_VA-API
    # may need to set the environment variable LIBVA_DRIVER_NAME=nvidia
}

install_vdpau_video_acceleration(){
    # vdpau is typically for older nvidia cards
    sudo dnf install \
        libva-utils \
        libva-vdpau-driver \
        vdpauinfo
    # https://wiki.archlinux.org/title/Hardware_video_acceleration#Configuring_VDPAU
}

uninstall_nvidia(){
    if [[ "N 3" != "$(runlevel)" ]]; then
        echo "Changing runlevel to multi-user"
        sudo systemctl set-default multi-user.target
        echo "Please reboot and run this command again"
        return 1
    fi
    sudo dnf remove \
        libva-nvidia-driver \
        libva-utils \
        libva-vdpau-driver \
        vdpauinfo
    installer=$(find /var/local/nvidia-installer -name 'NVIDIA-*.run' | sort -V | tail -n 1)
    if [[ ! -x "${installer}" ]]; then
        echo "WARNING: No installer found in /var/local/nvidia-installer. Skipping uninstall."
        return
    fi
    sudo "${installer}" --uninstall
}

help(){
    # Display Help
    echo "Multi step script to installing the NVIDA drivers."
    echo
    echo "Syntax: $0 [-a|d|e|g|h|i|n|v]"
    echo
    echo "options:"
    echo "-h     Print this help."
    echo "-d     Step1: Install dependencies."
    echo "-g     Step2: Generate driver signing key for secure boot."
    echo "-e     Step3: Enroll driver signing key."
    echo "-n     Step4: Disable nouveau driver."
    echo "-i     Step5: Install NVIDIA driver."
    echo "-a     Step6: Install VAAPI video acceleration support (newer cards)."
    echo "-v     Step6: Install VDPAU video acceleration support (older cards)."
    echo ""
    echo "-r     Restore nouveau driver"
}

while getopts ":adeghinrv" option; do
    case $option in
        a)
            install_vaapi_video_acceleration
            exit
        ;;
        d)
            install_dependencies
            exit
        ;;
        e)
            enroll_signing_key
            exit
        ;;
        g)
            generate_signing_key
            exit
        ;;
        h) # display Help
            help
            exit
        ;;
        i)
            install_nvidia
            exit
        ;;
        n)
            disable_nouveau
            exit
        ;;
        r)
            uninstall_nvidia
            enable_nouveau
            exit
        ;;
        v)
            install_vdpau_video_acceleration
            exit
        ;;
        *)
            echo failed
            exit 1
    esac
done

help
