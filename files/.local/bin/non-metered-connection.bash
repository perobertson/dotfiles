#!/usr/bin/env bash
set -euo pipefail

# nmcli is not easily available on WSL
if [ -z "$(command -v nmcli)" ]; then
    echo "nmcli is not installed. Assuming non metered connection"
    exit 0
fi

# Do not run on metered connections
device=$(ip route list default | cut -d' ' -f5)
if [ -z "$device" ]
then
    echo "No default device found. Are you connected?"
    exit 1
elif nmcli -g GENERAL.METERED device show "$device" | grep '^yes' &>/dev/null
then
    echo "Metered connection detected"
    exit 2
fi
