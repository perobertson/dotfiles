#!/usr/bin/env bash
# This file is generated by perobertson/dotfiles
# Manual edits will be lost in future updates
#
# k9s is a TUI for working with kubernetes
# https://k9scli.io/
set -euo pipefail

K9S_VERSION=$(curl -sSi "https://github.com/derailed/k9s/releases/latest" \
    | grep ^location \
    | tr -d '[:cntrl:]' \
    | sed 's/.*tag\/v//'
)

if [[ -x "${HOME}/.local/bin/k9s" ]] && k9s version 2>&1 | grep "Version:.*v${K9S_VERSION}" >/dev/null; then
    echo "SKIPPING: k9s is already installed"
    exit 0
fi

arch=$(uname -m)
if [[ "${arch}" == "x86_64" ]]; then
    arch="amd64"
elif [[ "${arch}" == "aarch64" ]]; then
    arch="arm64"
fi

filename=k9s_Linux_${arch}.tar.gz
curl -sSLo "/tmp/${filename}" "https://github.com/derailed/k9s/releases/download/v${K9S_VERSION}/${filename}"
cd /tmp
tar xzf "${filename}" k9s
install --mode 755 k9s "${HOME}/.local/bin/k9s"

echo "Installed k9s v${K9S_VERSION}"
