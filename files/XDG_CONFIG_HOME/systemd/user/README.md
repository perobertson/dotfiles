# Systemd

```mermaid
sequenceDiagram
par 15s
    boot-)user-updates: timer
    boot-)user-updates-update: timer
    boot-)rust-tools-update: timer
and 1min
    boot-)dotfiles-update: timer
    boot-)flatpak-update: timer
    boot-)granted-update: timer
    boot-)pkenv-update: timer
    boot-)pyenv-update: timer
    boot-)python-requirements-update: timer
    boot-)rbenv-update: timer
and 2min
    user-updates-update-->boot: finish ~2m10s
end
```
