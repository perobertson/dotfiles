# ~/.local/lib is the preferred place for applications
# https://www.freedesktop.org/software/systemd/man/file-hierarchy.html#Home%20Directory

# https://krew.sigs.k8s.io/
KREW_ROOT="${HOME}/.local/lib/krew"
# https://github.com/iamhsa/pkenv
PKENV_ROOT="${HOME}/.local/lib/pkenv"
# https://github.com/pyenv/pyenv
PYENV_ROOT="${HOME}/.local/lib/pyenv"
# https://github.com/rbenv/rbenv
RBENV_ROOT="${HOME}/.local/lib/rbenv"

export KREW_ROOT
export PKENV_ROOT
export PYENV_ROOT
export RBENV_ROOT

export GOPATH="${CODE_PATH}/go"

# virtual environments will modify the path then reopen a shell
# this will make sure we do not clobber the virtual env path
if [[ -z "${DOTFILES_PATH:-}" ]]; then
    # Prefer local bins over system
    if [[ -d "$HOME/.local/bin" ]]; then
        export PATH="$HOME/.local/bin:$PATH"
    fi

    # Prefer go bins over local
    if [[ -d "$GOPATH/bin" ]]; then
        export PATH="$GOPATH/bin:$PATH"
    fi

    # Prefer virtual envs over go bins
    if [[ -d "$KREW_ROOT/bin" ]]; then
        export PATH="$KREW_ROOT/bin:$PATH"
    fi
    if [[ -d "$RBENV_ROOT/bin" ]]; then
        export PATH="$RBENV_ROOT/bin:$PATH"
    fi
    if [[ -d "$PKENV_ROOT/bin" ]]; then
        export PATH="$PKENV_ROOT/bin:$PATH"
    fi
    if [[ -d "$PYENV_ROOT/bin" ]]; then
        export PATH="$PYENV_ROOT/bin:$PATH"
    fi

    # Prefer cargo bins over virtual envs
    if [[ -d "$HOME/.cargo/bin" ]]; then
        export PATH="$HOME/.cargo/bin:$PATH"
    fi

    # Set up rbenv
    if [[ -x "$(command -v rbenv)" ]]; then
        eval "$(rbenv init -)"
    fi

    # Set up pyenv
    if [[ -x "$(command -v pyenv)" ]]; then
        eval "$(pyenv init --path)"
    fi

    # Top level user bins override all else
    if [[ -d "$HOME/bin" ]]; then
        export PATH="$HOME/bin:$PATH"
    fi

    # Flag to prevent resetting the path in a subshell
    export DOTFILES_PATH=1
fi
