# Disable the paging of AWS cli output
export AWS_PAGER=""
# Where source code is stored
export CODE_PATH="$HOME/workspace"
# Default editor to use
export EDITOR='vim'
# Where go binaries get installed
export GOBIN="${HOME}/.local/bin"
# Where tenv will install the binaries to use
export TENV_ROOT="${XDG_STATE_HOME:-${HOME}/.local/state}/tenv"
if [[ ! -d "${TENV_ROOT}" ]]; then
    mkdir -p "${TENV_ROOT}"
fi
# Auto install detected tofu versions
export TENV_AUTO_INSTALL=true

# Configure less as the pager and reprint the screen when content changes
# Enables scrolling
export PAGER=less
export LESS=-R
