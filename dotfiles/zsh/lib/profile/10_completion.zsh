# generate completions for a command
# requires the command to support this format "$cmd completion zsh"
__dotfiles_generate_command_completion() {
    local cmd=${1:?command is required}
    if [[ "$cmd" != "$(basename "$cmd")" ]]; then
        echo "ERROR: absolute paths are not supported for completion generation" >&2
    elif [[ "$cmd" != "${cmd// /}" ]]; then
        echo "ERROR: commands with spaces are not supported for completion generation" >&2
    elif [[ -x "$(command -v "$cmd")" ]]; then
        "$cmd" completion zsh > "$HOME/.local/share/zsh/site-functions/_$cmd"
    elif [[ -r "$HOME/.local/share/zsh/site-functions/_$cmd" ]]; then
        rm "$HOME/.local/share/zsh/site-functions/_$cmd"
    fi
}

# similar to above but uses "completions --shell zsh" instead
__dotfiles_generate_command_completions_shell() {
    local cmd=${1:?command is required}
    if [[ "$cmd" != "$(basename "$cmd")" ]]; then
        echo "ERROR: absolute paths are not supported for completion generation" >&2
    elif [[ "$cmd" != "${cmd// /}" ]]; then
        echo "ERROR: commands with spaces are not supported for completion generation" >&2
    elif [[ -x "$(command -v "$cmd")" ]]; then
        "$cmd" completions --shell zsh > "$HOME/.local/share/zsh/site-functions/_$cmd"
    elif [[ -r "$HOME/.local/share/zsh/site-functions/_$cmd" ]]; then
        rm "$HOME/.local/share/zsh/site-functions/_$cmd"
    fi
}

# similar to above but uses "shell-completion zsh" instead
__dotfiles_generate_command_shell_completion() {
    local cmd=${1:?command is required}
    if [[ "$cmd" != "$(basename "$cmd")" ]]; then
        echo "ERROR: absolute paths are not supported for completion generation" >&2
    elif [[ "$cmd" != "${cmd// /}" ]]; then
        echo "ERROR: commands with spaces are not supported for completion generation" >&2
    elif [[ -x "$(command -v "$cmd")" ]]; then
        "$cmd" shell-completion zsh > "$HOME/.local/share/zsh/site-functions/_$cmd"
    elif [[ -r "$HOME/.local/share/zsh/site-functions/_$cmd" ]]; then
        rm "$HOME/.local/share/zsh/site-functions/_$cmd"
    fi
}

# similar to above but uses "--auto-complete-shell zsh" instead
__dotfiles_generate_command_auto_complete_shell() {
    local cmd=${1:?command is required}
    if [[ "$cmd" != "$(basename "$cmd")" ]]; then
        echo "ERROR: absolute paths are not supported for completion generation" >&2
    elif [[ "$cmd" != "${cmd// /}" ]]; then
        echo "ERROR: commands with spaces are not supported for completion generation" >&2
    elif [[ -x "$(command -v "$cmd")" ]]; then
        "$cmd" --auto-complete-shell zsh > "$HOME/.local/share/zsh/site-functions/_$cmd"
    elif [[ -r "$HOME/.local/share/zsh/site-functions/_$cmd" ]]; then
        rm "$HOME/.local/share/zsh/site-functions/_$cmd"
    fi
}

if [[ ! -d $HOME/.local/share/zsh/site-functions ]]; then
    mkdir -p "$HOME/.local/share/zsh/site-functions"
fi

__dotfiles_generate_command_completion assistant
__dotfiles_generate_command_completion cobra-cli
__dotfiles_generate_command_completion flux
__dotfiles_generate_command_completion helm
__dotfiles_generate_command_completion k9s
__dotfiles_generate_command_completion kubectl
__dotfiles_generate_command_completion kubectl-minio
__dotfiles_generate_command_completion kustomize
__dotfiles_generate_command_completion minikube
__dotfiles_generate_command_completion op
__dotfiles_generate_command_completion op.exe
__dotfiles_generate_command_completion pluto
__dotfiles_generate_command_completion scm-tools
__dotfiles_generate_command_completion sedo
__dotfiles_generate_command_completion spacectl
__dotfiles_generate_command_completion steampipe
__dotfiles_generate_command_completion tenv
__dotfiles_generate_command_completion user-updates

__dotfiles_generate_command_completions_shell oatmeal

__dotfiles_generate_command_shell_completion "yq"

__dotfiles_generate_command_auto_complete_shell "s3sync"
