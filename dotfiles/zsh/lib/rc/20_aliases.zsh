# The test for `! -x "$(command -v cmd)"` is done to prevent overriding packages
# This also helps keep the autocomplete list shorter when commands are not installed

# assume | granted
if [ -x "$(command -v assume)" ]; then
    # This is part of the assume setup. The output needs to be sourced
    alias assume="source \"$(command -v assume)\""
fi

# aws
if [ -x "$(command -v aws)" ]; then
    if [ ! -x "$(command -v aws-whoami)" ]; then
        alias aws-whoami="aws sts get-caller-identity"
    fi
fi

# cargo
if [ -x "$(command -v cargo)" ] && [ -x "$(command -v nice)" ]; then
    # run cargo with a lower scheduling priority
    alias cargo='nice cargo'
fi

# cat
if [ -x "$(command -v bat)" ]; then
    unalias -m 'cat'
    alias cat='bat -pp'
fi

# cd
alias ..='cd ..'

# code
if [ -x "$(command -v code)" ] && [ -x "$(command -v nice)" ]; then
    # run VSCode with a lower scheduling priority
    alias code='nice code'
fi

# containers
# when podman is installed on a system it is the default
# dive however defaults to using docker
if [ -x "$(command -v dive)" ] && [ -x "$(command -v podman)" ]; then
    alias dive="dive --source=podman"
fi

# git
if [ -x "$(command -v git)" ]; then
    alias g='git'
    alias ga='git add'
    alias gb='git branch'
    alias gbdm='git branch -d $(git branch --merged | grep -v ^\*)'
    alias gc='git commit'
    alias gcb='git cb'
    alias gcm='git cm'
    alias gco='git co'
    alias gd='git diff'
    alias gdc='git diff --cached'
    alias gdw='git diff --word-diff'
    alias gf='git fetch'
    alias gfa='git fetch --all --prune --prune-tags'
    alias gk='gitk --all --branches'
    alias gl='git pull --rebase --stat'
    alias glog='git log --oneline --decorate --color --graph'
    alias gloga='git log --graph --decorate --oneline --color --all'
    alias glogd='git log --graph --decorate $(git rev-list -g --all)'
    alias gm='git merge'
    alias gp='git push'
    alias gr='git remote'
    alias grb='git rebase'
    alias grba='git rebase --abort'
    alias grbc='git rebase --continue'
    alias grbi='git rebase -i'
    alias grbs='git rebase --skip'
    alias gs='git status'
    alias gss='git status --short'
fi

# go-task
if [ -x "$(command -v go-task)" ]; then
    if [ ! -x "$(command -v gt)" ]; then
        alias gt=go-task
    fi
    if [ ! -x "$(command -v task)" ]; then
        alias task=go-task
    fi
fi

# ls
alias ls="ls -F --color=auto"
alias l="ls -lAh"
alias ll="ls -l"
alias la='ls -A'
if [ -x "$(command -v eza)" ]; then
    unalias -m 'ls'
    unalias -m 'l'
    unalias -m 'll'
    unalias -m 'la'
    # eza -l has less info than ls -l so extra fileds are added back
    alias ls='eza --color auto --sort type --classify'
    alias l="ls -lagH"
    alias la="ls -a"
    alias ll="ls -lgH --git"
    alias lg="ls -lgHG"
fi

# packer
if [ -x "$(command -v packer)" ]; then
    if [ ! -x "$(command -v pkr)" ]; then
        alias pkr=packer
    fi
fi

# podman
if [ -x "$(command -v podman)" ]; then
    if [ ! -x "$(command -v pm)" ]; then
        alias pm=podman
    fi
fi

# rsync
if [ -x "$(command -v rsync)" ]; then
    alias rsync="rsync --progress"
fi

# ssh-keygen
if [ -x "$(command -v ssh-keygen)" ]; then
    alias ssh-keygen='ssh-keygen -t ed25519'
fi
