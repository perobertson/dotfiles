# https://zsh.sourceforge.io/Doc/Release/Completion-System.html
# use 'zstyle -L' to see all configured entries

# man zshparam
# A list of non-alphanumeric characters
# considered part of a word by the line editor
# WORDCHARS=*?_-.[]~=/&;!#$%^(){}<>
WORDCHARS=''

# menu selecting
zstyle ':completion:*:*:*:*:*' menu select

# matches case insensitive for lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'

# pasting with tabs doesn't perform completion
zstyle ':completion:*' insert-tab pending

# completion style for makefiles
zstyle ':completion:*:*:make:*' tag-order 'targets'

# colorful completion list
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# Configure the directories where completions and functions are defined
# Different distributions put them in different locations
# /usr/share/zsh-completion/completions - kubectl(fedora)
# /usr/share/zsh/vendor-completions - debian,ubuntu
# /usr/local/share/zsh/vendor-completions - debian,ubuntu
# $HOME/.zsh/functions - dotfiles
# $HOME/.local/share/zsh/site-functions - dotfiles
fpath=($HOME/.zsh/functions $HOME/.local/share/zsh/site-functions /usr/local/share/zsh/vendor-completions /usr/share/zsh/vendor-completions /usr/share/zsh-completion/completions $fpath)

# Initialize the completions, defines compdef
autoload -Uz compinit
compinit

# Autoload the functions so they can be executed when called
# This allows for the files that do not start with _ to be called as a function
autoload -Uz $fpath[1]/*(:t)

# Completions for pip
if [[ -x "$(command -v pip)" ]]; then
    compctl -K _pip_completion pip
fi
if [[ -x "$(command -v pip3)" ]]; then
    compctl -K _pip_completion pip3
fi

# Completions for pyenv
if [[ -r "$PYENV_ROOT/completions/pyenv.zsh" ]]; then
    source "$PYENV_ROOT/completions/pyenv.zsh"
fi

# Configure starship prompt
if [[ -x "$(command -v starship)" ]]; then
    eval "$(starship init zsh)"
fi

# Initialize terraform completions when a global version is set
if [[ -f "${TENV_ROOT}/Terraform/version" ]]; then
    autoload -U +X bashcompinit && bashcompinit
    complete -o nospace -C "${TENV_ROOT}/Terraform/$(cat "${TENV_ROOT}/Terraform/version")/terraform" terraform
fi

# Initialize tofu completions when a global version is set
if [[ -f "${TENV_ROOT}/OpenTofu/version" ]]; then
    autoload -U +X bashcompinit && bashcompinit
    complete -o nospace -C "${TENV_ROOT}/OpenTofu/$(cat "${TENV_ROOT}/OpenTofu/version")/tofu" tofu
fi

# Set up zoxide
if [[ -x "$(command -v zoxide)" ]]; then
    eval "$(zoxide init zsh)"
fi
