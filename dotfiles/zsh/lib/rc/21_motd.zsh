__motd_clear_cache(){
    rm -f "${XDG_CACHE_HOME:-$HOME/.cache}/dotfiles/motd_check"
}

__motd_display(){
    date +%s > "${XDG_CACHE_HOME:-$HOME/.cache}/dotfiles/motd_check"
    echo -e "MotD: $(stat --format="%y" /etc/motd)\n"
    cat /etc/motd
}

motd(){
    if [[ ! -r /etc/motd ]]; then
        return
    fi
    cache_dir=${XDG_CACHE_HOME:-$HOME/.cache}/dotfiles
    if [[ -r "${cache_dir}/motd_check" ]]; then
        last_displayed=$(cat "${cache_dir}/motd_check")
        motd_updated=$(stat --format="%Y" /etc/motd)
        if [[ "${motd_updated}" > "${last_displayed}" ]]; then
            __motd_display
        fi
    else
        mkdir -p "${cache_dir}"
        __motd_display
    fi
}
