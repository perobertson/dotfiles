typeset -A _dotfiles_op_signin
op-signin () {
    local account=${1:-$OP_ACCOUNT}
    if [[ -z $account ]]; then
        if [[ 1 -ne $(/usr/bin/op account list --format json|jq '.|length') ]]; then
            echo "ERROR: arg1: account is required when there is more than 1 available"
            return 1
        else
            account=$(/usr/bin/op account list --format json|jq -r '.[].account_uuid')
            export OP_ACCOUNT=$account
        fi
    fi
    while ! /usr/bin/op whoami --account "$account" &>/dev/null
    do
        _dotfiles_op_signin[$account]=0
        eval "$(/usr/bin/op signin --account "$account")"
    done
    # update after signin for when the account has changed
    export OP_ACCOUNT=$account
    # keep track of how many times signin happened
    ((_dotfiles_op_signin[$account]++))
    return 0
}

op-signout () {
    local account=${1:-$OP_ACCOUNT}
    if [[ -z $account ]]; then
        if [[ 1 -ne $(/usr/bin/op account list --format json|jq '.|length') ]]; then
            echo "ERROR: arg1: account is required when there is more than 1 available"
            return 1
        else
            account=$(/usr/bin/op account list --format json|jq -r '.[].account_uuid')
        fi
    fi
    if [[ "${_dotfiles_op_signin[$account]:=0}" == 0 ]]; then
        return 0
    elif [[ "${_dotfiles_op_signin[$account]}" == 1 ]]; then
        /usr/bin/op signout --account "$account"
    fi
    # keep track of how many times signin happened
    ((_dotfiles_op_signin[$account]--))
    return 0
}

op-env () {
    # start a subshell and populate environment variables from a secure note
    # MY_VAR=op://vault-name/item-name/[section-name/]field-name
    # https://developer.1password.com/docs/cli/secrets-reference-syntax
    local account=${1:-$OP_ACCOUNT}
    local env_item=${2:-op-env}
    if [[ -z $account ]]; then
        if [[ 1 -ne $(/usr/bin/op account list --format json|jq '.|length') ]]; then
            echo "ERROR: arg1: account is required when there is more than 1 available"
            return 1
        else
            account=$(/usr/bin/op account list --format json|jq -r '.[].account_uuid')
            export OP_ACCOUNT=$account
        fi
    fi
    if [[ -z $env_item ]]; then
        echo "ERROR: arg2: env_item is required"
        return 1
    fi

    op-signin "${account}"
    # no-masking is needed otherwise commands inside the shell will hang
    /usr/bin/op run \
        --account "${account}" \
        --env-file <(/usr/bin/op item get "${env_item}" \
            --fields=notesplain \
            --format=json \
            | jq -r .value) \
        --no-masking \
        -- "${SHELL:-zsh}"
    op-signout "${account}"
}

# The desktop application handles the signin logic
# settings > developer > integrate with cli; enabled
op-env-desktop-auth () {
    local env_item=${1:-op-env}
    /usr/bin/op run \
        --env-file <(/usr/bin/op item get "${env_item}" \
            --fields=notesplain \
            --format=json \
            | jq -r .value) \
        --no-masking \
        -- "${SHELL:-zsh}"
}

# aws-vault sets AWS_SECURITY_TOKEN but that was replaced by AWS_SESSION_TOKEN in 2014
# we unset it here because this function will launch a subshell and source this file again
# https://aws.amazon.com/blogs/security/a-new-and-standardized-way-to-manage-credentials-in-the-aws-sdks/
unset AWS_SECURITY_TOKEN
aws-op-env () {
    # Like op-env and will start a subshell with environment variables set.
    # This uses aws-vault for credential management to create temporary sessions
    # https://github.com/99designs/aws-vault
    local aws_profile=${1}
    local op_account=${2}
    local op_vault=${3:-private}
    local op_aws_item=${4:-aws}
    local op_env_item=${5:-op-env}

    if [[ -z $aws_profile ]]; then
        echo "ERROR: arg1: aws_profile is required"
        return 1
    elif ! grep "^\[profile ${aws_profile}\]$" ~/.aws/config &>/dev/null; then
        echo "ERROR: profile ${aws_profile} is not configured." \
             "Did you forget to setup ~/.aws/config"
        return 1
    fi
    if [[ -z $op_account ]]; then
        echo "ERROR: arg2: op_account is required"
        return 1
    fi
    if [[ -z $op_vault ]]; then
        echo "ERROR: arg3: op_vault is required"
        return 1
    fi
    if [[ -z $op_aws_item ]]; then
        echo "ERROR: arg4: op_aws_item is required"
        return 1
    fi
    if [[ -z $op_env_item ]]; then
        echo "ERROR: arg5: op_env_item is required"
        return 1
    fi

    # clear cached sessions so the duration is deterministic
    aws-vault clear "${aws_profile}" &>/dev/null

    local aws_mfa
    op-signin "${op_account}"
    aws_mfa=$(/usr/bin/op item get --otp --vault=${op_vault} ${op_aws_item})

    # flag to unset the AWS_REGION variables when starting the subshell
    # needed because aws-vault sets them only if it needs to log in
    # this causes unpredictible usage, so always unset
    export DOTFILES_UNSET_AWS_REGION=1

    # no-masking is needed otherwise commands inside the shell will hang
    aws-vault exec "${aws_profile}" -d 12h --region=ca-central-1 \
        --mfa-token="${aws_mfa}" \
        -- \
        /usr/bin/op run \
            --account "${op_account}" \
            --env-file <(/usr/bin/op item get "${op_env_item}" \
                --fields=notesplain \
                --format=json \
                | jq -r .value) \
            --no-masking \
            -- "${SHELL:-zsh}"

    op-signout "${op_account}"

    unset DOTFILES_UNSET_AWS_REGION
}

aws_saml-op-env () {
    # Like op-env and will start a subshell with environment variables set.
    # This uses saml2aws for credential management to create temporary sessions
    # This approach is required when an identity provider like Okta is required
    # https://github.com/Versent/saml2aws
    local aws_profile=$1
    local aws_role_arn=$2
    local op_account=$3
    local op_vault=$4
    local op_saml_item=$5
    local session_duration=${6:-43200}

    if [[ -z $aws_profile ]]; then
        echo "ERROR: arg1: aws_profile is required"
        return 1
    fi
    if [[ -z $aws_role_arn ]]; then
        echo "ERROR: arg2: aws_role_arn is required"
        return 1
    fi
    if [[ -z $op_account ]]; then
        echo "ERROR: arg3: op_account is required"
        return 1
    fi
    if [[ -z $op_vault ]]; then
        echo "ERROR: arg4: op_vault is required"
        return 1
    fi
    if [[ -z $op_saml_item ]]; then
        echo "ERROR: arg5: op_saml_item is required"
        return 1
    fi

    op-signin "${op_account}"

    local saml_mfa
    saml_mfa=$(/usr/bin/op item get --otp --vault=${op_vault} ${op_saml_item})

    saml2aws login \
        --skip-prompt \
        --profile="${aws_profile}" \
        --mfa-token="${saml_mfa}" \
        --role="${aws_role_arn}" \
        --session-duration="${session_duration}"

    export AWS_PROFILE="${aws_profile}"
    op-env "${op_account}"
    unset AWS_PROFILE

    op-signout "${op_account}"
}

if [[ -n $DOTFILES_UNSET_AWS_REGION ]]; then
    unset DOTFILES_UNSET_AWS_REGION
    unset AWS_DEFAULT_REGION
    unset AWS_REGION
fi
