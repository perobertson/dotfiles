#!/usr/bin/env python3
"""Setup script for installing dotfiles on a new computer."""
from __future__ import generator_stop

import argparse
import logging
import subprocess  # noqa: S404
import sys
from pathlib import Path

log = logging.getLogger(__name__)


def _link_file(
    link: Path,
    target: Path,
    dry_run: bool = False,
    backup: bool = False,
) -> None:
    """Create a symlink at link to target.

    :param link:    the name of the link to create
    :type link:     pathlib.Path
    :param target:  the target that the link points to
    :type target:   pathlib.Path
    """
    log.info("linking file '%s' -> '%s'", link, target)
    if not dry_run:
        if backup:
            link_uri = link.resolve()
            backup_uri = f"{link_uri}.bak"
            log.info("created backup '%s'", backup_uri)
            link.replace(backup_uri)
        link.symlink_to(target, target_is_directory=target.is_dir())


def _install_gitconfig(script: Path, dry_run: bool = False) -> None:
    """Install the global gitconfig file.

    This file is special because we want some sections to be untracked.
    Use include to handle this case https://git-scm.com/docs/git-config#_includes
    """
    global_config_file = Path("~").joinpath(".gitconfig").expanduser()
    dotfiles_dir = script.parent.joinpath("dotfiles")

    source_global_config_file = dotfiles_dir.joinpath("git-global.ini")
    include_global_config_path = f"path = {source_global_config_file.resolve()}"
    include_global_config = f"[include]\n\t{include_global_config_path}"

    source_delta_config_file = dotfiles_dir.joinpath("git-delta.ini")
    include_delta_config_path = f"path = {source_delta_config_file.resolve()}"
    include_delta_config = f"[include]\n\t{include_delta_config_path}"

    if global_config_file.exists():
        log.debug("global gitconfig exists")
        with global_config_file.open() as f:
            content = f.read()
        if include_global_config_path not in content:
            log.info("\tappending global include section")
            if not dry_run:
                with global_config_file.open(mode="a") as f:
                    print(include_global_config, file=f)
        if include_delta_config_path not in content:
            log.info("\tappending delta include section")
            if not dry_run:
                with global_config_file.open(mode="a") as f:
                    print(include_delta_config, file=f)
    else:
        log.info("creating %s", global_config_file)
        if not dry_run:
            with global_config_file.open(mode="w") as f:
                print(include_global_config, file=f)
                print(include_delta_config, file=f)


def _install_dotfiles(script: Path, dry_run: bool = False) -> None:
    """Install the dotfiles.

    :param script: Path object to this file
    :type script: pathlib.Path
    :param dry_run: True if the commands should be displayed instead of executed
    """
    log.info("Installing dotfiles")
    to_skip = {
        ".git",
        ".gitignore",
        ".gitmodules",
        "install.py",
        "LICENSE",
        "Rakefile",
        "README.md",
        "README.rdoc",
    }
    script.joinpath
    dotfiles = script.parent.joinpath("dotfiles")
    for f in dotfiles.iterdir():
        if f.name in to_skip:
            log.debug("skipping: %s", f.name)
            continue
        home_dir_file = Path("~").joinpath(f".{f.name}").expanduser()
        if home_dir_file.exists():
            if f.samefile(home_dir_file):
                log.debug("identical %s", home_dir_file)
            else:
                _link_file(home_dir_file, f, dry_run=dry_run, backup=True)
        else:
            _link_file(home_dir_file, f, dry_run=dry_run)
    _install_gitconfig(script, dry_run=dry_run)


def _link_files(local_dir: Path, target_dir: Path, dry_run: bool = False) -> None:
    """Link files in the local dir to the target dir.

    This will only create symlinks for files to avoid pulling in additional configs.
    """
    # make sure the directory exists in the home path
    local_dir.mkdir(parents=True, exist_ok=True)

    for target in target_dir.iterdir():
        if target.is_dir():
            _link_files(local_dir.joinpath(target.name), target, dry_run)
        elif target.is_file():
            config_file = local_dir.joinpath(target.name)

            is_file = config_file.is_file()
            is_link = config_file.is_symlink()
            is_same = (is_file or is_link) and config_file.samefile(target)

            msg = (
                f"file://{config_file}\n"
                f"  is_file:{is_file}\n"
                f"  is_symlink:{is_link}\n"
                f"  samefile:{is_same}"
            )
            log.debug(msg)

            if is_same:
                continue

            if is_file:
                backup_uri = f"{config_file.resolve()}.bak"
                backup_path = Path(backup_uri)
                log.info("Creating backup: %s", backup_path)
                if not dry_run:
                    config_file.replace(backup_path)
            log.info("Creating link: %s -> %s", config_file, target)
            if not dry_run:
                config_file.symlink_to(target, target_is_directory=target.is_dir())


def _fetch_submodules(script: Path, dry_run: bool = False) -> None:
    log.info("initializing submodules")
    if dry_run:
        log.debug("git submodule init")
        log.debug("git submodule update")
    else:
        cwd = str(script.parent.resolve())
        cmd = ["git", "submodule", "init"]
        subprocess.run(cmd, shell=False, check=True, cwd=cwd)  # noqa: S603
        cmd = ["git", "submodule", "update"]
        subprocess.run(cmd, shell=False, check=True, cwd=cwd)  # noqa: S603


def _init_logging(args: argparse.Namespace) -> None:
    """Initialize the logger."""
    log_fmt = (
        "%(asctime)-s %(levelname)-8s %(name)s:%(lineno)s(%(funcName)s) %(message)s"
    )
    logging.basicConfig(format=log_fmt)
    if args.dry_run:
        log.setLevel(logging.DEBUG)
    elif args.verbose == 1:
        log.setLevel(logging.INFO)
    elif args.verbose > 1:
        log.setLevel(logging.DEBUG)


def main() -> None:
    """Install the dotfiles and initialize submodules."""
    script_path = Path(sys.argv[0])
    script = script_path.resolve()

    parser = argparse.ArgumentParser()
    parser.add_argument("--dry-run", action="store_true", default=False)
    parser.add_argument("--verbose", "-v", action="count", default=0)
    args = parser.parse_args()

    _init_logging(args)

    log.debug("starting")
    _fetch_submodules(script, dry_run=args.dry_run)
    _install_dotfiles(script, dry_run=args.dry_run)
    print("work complete")


if __name__ == "__main__":
    main()
