# Paul Robertson Dot Files

These are config files to set up a system the way I like it.

## Installation

```bash
git clone https://gitlab.com/perobertson/dotfiles.git ~/.dotfiles
cd ~/.dotfiles
make install
```

## Environment

I primarily use Linux with `zsh` as my shell.
To switch to zsh, you can do so with the following command.

```bash
chsh -s "$(command -v zsh)"
```

## Features

I normally place all of my coding projects in `~/workspace`.
This directory can easily be accessed (and tab completed) with the `c` command.
This can be configured by exporting `CODE_PATH` from the `~/.localrc` file.

```text
c re<tab>
```

There is also an "h" command which behaves similar, but acts on the
home path.

```text
h doc<tab>
```

You can see the other functions that are defined in `~/.zsh/functions/`.
The aliases can be found in `~/.zsh/lib/20_aliases.zsh`.

If there are some shell configuration settings which you want specific to
one system, place it into a `~/.localrc` file. This will be loaded automatically
if it exists.

## Optional Features

These dotfiles are meant to work with what is available on the system and will
be improved by having additional programs installed.

- bat
- eza
- starship

PATH is also setup to prefer some binaries over others.
Preference is in this order:

1. ~/bin
1. $HOME/.cargo/bin
1. $PYENV_ROOT/bin
1. $PKENV_ROOT/bin
1. $RBENV_ROOT/bin
1. $KREW_ROOT/bin
1. $GOPATH/bin
1. ~/.local/bin
1. system
